<!DOCTYPE html>
<html lang="ru">
<head>
	<title>Web 7</title>
	<style>
	   .error {
            border: 2px solid red;
        }
	</style>
	<link rel="stylesheet" 
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    <?php
    if (!empty($messages)) {
      print('<div id="messages">');
      foreach ($messages as $message) {
        print($message);
      }
      print('</div>');
    }
    ?>
	<div class="w-50 mx-auto my-5 rounded bg-white">
	<?php if(!empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
                 echo '<div class="">
                          <form action="" method="POST" class="rounded bg-white">
                          <input name="requestMe" type="hidden" value="LO" />
                          <button type="submit" class="btn btn-danger">Log out</button>
                          </form>
                       </div>';
              } else{
                 echo '<div>
                          <form action="" method="POST" class="rounded bg-white">
                          <input name="requestMe" type="hidden" value="LI" />
                          <div class="d-flex justify-content-between">
                              <label>Have account?</label>
                              <button type="submit" class="btn btn-success">Log in</button>
                          </div>
                          </form>
                       </div>'; 
              }
    ?>
    	<a href="./admin.php" class="rounded bg-white">Are you admin?</a>
		<form action="" method="POST" class="rounded bg-white">
			<div class="from-group d-flex flex-column">
				<div class="my-2">
					<label>Name</label>
					<div class="col-sm-10">
        				<input type="text" name="Name" class="form-control" 
        				<?php if ($errors['Name']) {print 'class="error"';} ?>  value="
        				<?php print $values['Name'];?>" />
      				</div>
      			</div>
      			<div class="my-2">
      				<label>Email</label>
      				<div class="col-sm-10">
        				<input type="email" class="form-control" name="Email" aria-describedby="emailHelp" 
        				<?php if ($errors['Email']) {print 'class="error"';} ?> value="
        				<?php print $values['Email']; ?>" />
      				</div>
      			</div>
      			<div class="my-2">
      				<label>Date of Birth</label>
      				<div class="d-flex flex-row justify-content-around">
      					<div class="d-flex flex-column">
      						<label>Day</label>
      						<input type="text" class="form-control" name="DD" 
      						<?php if ($errors['DD']) {print 'class="error"';} ?> value="
      						<?php print $values['DD']; ?>">
      					</div>
      					<div class="d-flex flex-column">
      						<label>Month</label>
      						<input type="text" class="form-control" name="DM" 
      						<?php if ($errors['DM']) {print 'class="error"';} ?> value="
      						<?php print $values['DM']; ?>">
      					</div>
      					<div class="d-flex flex-column">
      						<label>Year</label>
      						<input type="number" class="form-control" name="DY" 
      						<?php if ($errors['DY']) {print 'class="error"';} ?> value="
      						<?php print $values['DY']; ?>">
      					</div>
      				</div>
      			</div>
      			<div class="my-2">
      				<label>Sex</label>
      				<div class="form-check">
        				<label class="form-check-label">
          					<input type="radio" class="form-check-input" name="Rad" 
          					id="SMale" value="MALE" <?php if ($values['PO']=='MALE') 
          					    print 'checked=""'; ?>>Male
       					</label>
      				</div>
      				<div class="form-check">
      					<label class="form-check-label">
          					<input type="radio" class="form-check-input" name="Rad" 
          					id="SFe" value="FEMALE" <?php if ($values['PO']=='FEMALE') 
          					    print 'checked=""'; ?>>Female
        				</label>
      				</div>
      			</div>
      			<div class="my-2">
      				<label>Number of limbs</label>
      				<div class="d-flex justify-content-between">
	      				<div class="form-check">
	        				<label class="form-check-label">
	          					<input type="radio" class="form-check-input" 
	          					name="Limbs" id="0" value="0" 
	          					<?php if ($values['LI']=='0') print 'checked=""'; ?>>0
	       					</label>
	      				</div>
	      				<div class="form-check">
	      					<label class="form-check-label">
	          					<input type="radio" class="form-check-input" 
	          					name="Limbs" id="1" value="1" 
	          					<?php if ($values['LI']=='1') print 'checked=""'; ?>>1
	        				</label>
	      				</div>
	      				<div class="form-check">
	      					<label class="form-check-label">
	          					<input type="radio" class="form-check-input" 
	          					name="Limbs" id="2" value="2" 
	          					<?php if ($values['LI']=='2') print 'checked=""'; ?>>2
	        				</label>
	      				</div>
	      				<div class="form-check">
	      					<label class="form-check-label">
	          					<input type="radio" class="form-check-input" 
	          					name="Limbs" id="3" value="3" 
	          					<?php if ($values['LI']=='3') print 'checked=""'; ?>>3
	        				</label>
	      				</div>
	      				<div class="form-check">
	      					<label class="form-check-label">
	          					<input type="radio" class="form-check-input" 
	          					name="Limbs" id="4" value="4" 
	          					<?php if ($values['LI']=='4') print 'checked=""'; ?>>4
	        				</label>
	      				</div>
	      			</div>
      			</div>
      			<div class="my-2 jumbotron p-1" 
      			<?php if ($errors['SP']) {print 'class="error"';} ?>>
      				<div class="form-group" value="">
      					<label for="exampleSelect2">Superpowers</label>
      					<select multiple="" class="form-control" name="SP[]">
        				<option <?php for ($i=0;$i<count($values, COUNT_RECURSIVE)-10;$i++) 
        				    if ($values['SP'][$i]=='Ability to split fire') print 'selected=""' ?>>
        				    Ability to split fire</option>
                		<option <?php for ($i=0;$i<count($values, COUNT_RECURSIVE)-10;$i++) 
                		    if ($values['SP'][$i]=='Learning to teleport') print 'selected=""' ?>>
                		    Learning to teleport</option>
                		<option <?php for ($i=0;$i<count($values, COUNT_RECURSIVE)-10;$i++) 
                		    if ($values['SP'][$i]=='Invisibility') print 'selected=""' ?>>
                		    Invisibility</option>
                		<option <?php for ($i=0;$i<count($values, COUNT_RECURSIVE)-10;$i++) 
                		    if ($values['SP'][$i]=='One-Punch Man') print 'selected=""' ?>>One-Punch Man</option>
				      </select>
				    </div>
      			</div>
      			<div class="my-2">
      				<div class="form-group">
      					<label>Biography</label>
      					<textarea class="form-control" name="BG" rows="3" 
      					<?php if ($errors['BG']) {print 'class="error"';} ?>>
      					<?php print $values['BG']; ?></textarea>
    				</div>	
      			</div>
      			<div class="my-2 <?php if ($errors['CH']) {print 'class="error"';} ?>">
      				<div class="form-check">
        				<label><input class="form-check-input" type="checkbox" name="CH" <?php if ($errors['CH']) {print 'class="error"';} ?>  value="Yes" <?php print $values['CH']; ?>>I checked contract
        				</labe>
      				</div>
      			</div>
      			<input name="requestMe" type="hidden" value="OaI" />
      			<button type="submit" class="btn btn-dark">Send</button>
			</div>
		</form>
	</div>
</body>
</html>
